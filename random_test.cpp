#include <bits/stdc++.h>
#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <time.h> 
using namespace std;

typedef long long ll;
#define fore(i,a,b) for(ll i = a ; i<b; i++)

int main(){

    ofstream fout("test/random_test.txt");
    srand(clock());
    ll n =  rand() % 1000;
    ll m = rand() %  (n*(n-1))/2;
    
    fout<<"p edge "<<n<<" "<<m<<endl;
    srand(clock());
    fore(i,0,m){
        ll v = rand() % n , w = rand() %n;
        if(v == w) w = rand() %n;
        fout<<"e "<<v<<" "<<w<<endl;
    }

    return 0;
} 