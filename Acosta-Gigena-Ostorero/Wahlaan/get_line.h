#ifndef _GET_LINE
#define _GET_LINE
#include "Rii.h"

typedef unsigned int u32;

char* get_line(void);
void get_numbers(char*, u32, u32*, u32*);
bool checkline(char*, bool);

#endif