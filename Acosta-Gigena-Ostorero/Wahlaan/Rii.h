#ifndef RII
#define RII
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "vector.h"
#include "grafo.h"
#include "stack.h"
#include "avl.h"
#include "get_line.h"

/**
 *  - Leandro Acosta leacosta97@gmail.com
 *  - Gonzalo Gigena gongigena@gmail.com
 *  - Mateo Ostorero mateoostorero71@gmail.com
 */

typedef unsigned int u32;
#define max(a, b) ((a > b) ? a : b)

#endif