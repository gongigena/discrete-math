## 1 Introduccion

```
Deber ́an implementar lo que se detalla a continuaci ́on en C (C99, i.e., pueden usar comentarios // u otras cosas de C99).
El proyecto puede ser hecho en forma individual o en grupos de 2 personas.
```
      - Proyecto de Matem ́atica Discreta II-
- 1 Introducci ́on Contents
   - 1.1 Restricciones generales
- 2 Entrega
   - 2.1 Fecha de Entrega
   - 2.2 Archivos del programa
   - 2.3 Protocolo
   - 2.4 Nota del Proyecto, Errores del programa y retorno
   - 2.5 Que pasa si el proyecto no es aprobado
   - 2.6 Duraci ́on de la aprobaci ́on
- 3 Tipos de datos
   - 3.1 u32:
   - 3.2 GrafoSt
   - 3.3 Grafo
- 4 Funciones De Construcci ́on/Destrucci ́on/Copia del grafo
   - 4.1 ConstruccionDelGrafo()
   - 4.2 DestruccionDelGrafo()
   - 4.3 CopiarGrafo()
- 5 Funciones de coloreo
   - 5.1 Greedy()
   - 5.2 Bipartito()
- 6 Funciones para extraer informaci ́on de datos del grafo
   - 6.1 NumeroDeVertices()
   - 6.2 NumeroDeLados()
   - 6.3 NumeroDeColores()
- 7 Funciones de los vertices
   - 7.1 NombreDelVertice()
   - 7.2 ColorDelVertice()
   - 7.3 GradoDelVertice()
   - 7.4 ColorJotaesimoVecino()
   - 7.5 NombreJotaesimoVecino()
- 8 Funciones de ordenaci ́on
   - 8.1 OrdenNatural()
   - 8.2 OrdenWelshPowell()
   - 8.3 SwitchVertices()
   - 8.4 RMBCnormal()
   - 8.5 RMBCrevierte()


```
8.6 RMBCchicogrande().................................................. 7
8.7 SwitchColores()..................................................... 7
```
9 Formato de Entrada 8

10 Final Warnings 9
10.1 Advertencias generales................................................. 9
10.2 Cosas que causan desaprobaci ́on autom ́atica..................................... 11
10.3 Cosas que pueden o no causar desaprobaci ́on del proyecto............................. 11
10.4 Cosas que no causan desaprobaci ́on autom ́atica pero si devoluci ́on del proyecto................. 12
10.5 No causa desaprobaci ́on ni devoluci ́on del proyecto pero si alg ́un descuento en la nota............. 12

### 1.1 Restricciones generales

El c ́odigo debe ser razonablemente portable. Ademas:
1) No pueden usar getline
2) No pueden usar archivos llamados aux.c o aux.h
3) No pueden tener archivos tales que la unica diferencia en su nombre sea diferencia en la capitalizaci ́on.
4) No pueden llamar a ninguna libreria que no sean las del estandard de C99.
5) No pueden tener archivos ni directorios que tengan un espacio en el nombre.
Pueden consultar con otros grupos, pero si vemos copiado de grandes fragmentos de c ́odigo la van a pasar mal. Y la van
a pasar peor si descubrimos intento de enga ̃narnos haciendo cambios cosm ́eticos en el c ́odigo de otro grupo.

## 2 Entrega

Deben entregar v ́ıa e-mail los archivos que implementan el proyecto. Los archivos del programa deben ser todos archivos .c
o .h. No debe haber ningun ejecutable. Si entregan un ejecutable el proyecto se devuelve autom ́aticamente sin corregir.

### 2.1 Fecha de Entrega

Martes 23 de abril de 2018, hasta las 09:00 hs. (9AM).

### 2.2 Archivos del programa

Deben entregar un archivo Rii.h en el cual se hagan las llamadas que uds. consideren convenientes a librerias generales de C,
ademas de las especificaciones de los tipos de datos y funciones descriptas en este documento. Tengan cuidado de poner los
nombres correctos, pues ese ser ́a el archivo que incluiremos en nuestros algoritmos de testeo, el cual usar ́a los nombres dados
ac ́a, asi que si los nombres no son los correctos, el program ́a no compilar ́a y ser ́an desaprobados.
No es necesario que todo este definido en Rii.h, pero si definen algo en otros archivos auxiliares la llamada a esos archivos
auxiliares debe estar dentro de Rii.h. En Rii.h debe figurar (comentados, obvio) los nombres y los mails de contacto de todos
los integrantes del grupo.
Junto con Rii.h obviamente deben entregar uno o mas archivos .c que implementen las funciones indicadas en este docu-
mento, pero pueden nombrarlos como quieran.
Deben empaquetar todo de la siguiente forma:
Debe haber un directorio de primer nivel cuyo nombre consiste en los apellidos de los integrantes.
En ese directorio deben poner cualquier informaci ́on extra que quieran agregar. Pueden no poner nada, pero si lo hacen
el formato debe ser .pdf o un archivo ASCII. Si entregan un .docx el proyecto se devuelve sin corregir.
Ademas, en ese directorio habr ́a un directorio de segundo nivel, llamado Wahlaan
En este directorio deben estar Rii.h y todos los otros archivos que necesiten, incluidos archivos auxiliares, pero no debe
haber ningun main.Todo .c que este en este directorio se considera que debe formar parte de la compilaci ́on.
(usaremos *.c en Wahlaan para compilar).
(Rii significa “Esencia”, Wahl es un verbo que significa “Crear/Construir”y Wahlaan es la forma pasada de ese verbo
“Construido/Creado”).
Empaqueten todo en formato .tgz y envienlo por mail a:
matematicadiscretaii.famaf arroba gmail.com
Compilaremos (con mains nuestros) desde el directorio de primer nivel con gcc, -IWahlaan , -Wall, -Wextra, -O3, -std=c
.


Esas flags seran usadas para testear la velocidad, pero para testear grafos chicos podemos agregar otras flags. Por ejemplo,
podemos usar -DNDEBUG si vemos que estan mal usando asserts. Tambien compilaremos, para testear grafos chicos, con
flags que nos permitan ver si hay buffer overflows, shadow variables o comportamientos indefinidos.

### 2.3 Protocolo

Luego de enviado, se les responder ́a con un “recibido”. Si no reciben confirmaci ́on dentro de las 24hs pregunten si lo recib ́ı.
(eso ahora, en la epoca de entrega. Si lo envian luego, pej en el segundo cuatrimestre, puedo tomarme mucho mas para
contestar)

### 2.4 Nota del Proyecto, Errores del programa y retorno

El Proyecto ser ́a evaluado con una nota entre 0 y 10, la cual sera promediada por medio de un promedio pesado con las notas
del Te ́orico y del Pr ́actico para obtener la nota final. Las tres partes deben aprobarse por separado, asi que si no aprueban
el proyecto no aprueban el final.
Dependiendo de los errores, podemos darles la nota definitiva del proyecto luego de corregido, o bien podemos devolverles
el proyecto para que corrijan algunos errores antes de fijar la nota, si es que consideramos estos errores como serios pero no
lo suficientemente serios como para desaprobar el proyecto. Toda devoluci ́on de proyecto implica descuento de puntos.
Si los errores son muy graves, el proyecto queda desaprobado.

### 2.5 Que pasa si el proyecto no es aprobado

Si el proyecto no es aprobado (o no lo entregan) tienen un “recuperatorio”:pueden entregar el proyecto hasta el mismo dia
del examen que quieran rendir, pero las notas posibles son solo 0,1,2,3 si desaprueban esa versi ́on o 4 si la aprueban. El
proyecto no se corrige a menos que efectivamente se presenten al examen y si no aprueban esa version no aprueban el final.
Esta opci ́on no esta disponible luego de la primera fecha de febrero de 2020. (es decir, para la segunda fecha de febrero o las
fechas de marzo en adelante, ya no tienen esta opci ́on, pero en la primera de febrero si). Si aprueban el proyecto pero no el
examen (pues no aprueban el te ́orico o el pr ́actico) el proyecto queda aprobado, no es necesario que lo reentreguen cuando
vuelvan a rendir.
Para las fechas de diciembre y febrero, deber ́an ademas entregar un archivo .c con un main que se especificar ́a mas adelante.
(ver mi pagina en agosto, luego de las fechas de examen de julio-agosto)

### 2.6 Duraci ́on de la aprobaci ́on

En principio, la aprobaci ́on del proyecto, ya sea durante la entrega normal o aprobado durante un examen, caduca en abril
del 2021, pero es posible que la extienda a abril del 2022.

## 3 Tipos de datos

### 3.1 u32:

Se utilizar ́a el tipo de datou32para especificar un entero de 32 bits sin signo.
Todos los enteros sin signo de 32 bits que aparezcan en la implementaci ́on deber ́an usar este tipo de dato.
Los grafos a colorear tendran una lista de lados cuyos vertices seran todos u32.

### 3.2 GrafoSt

Es una estructura, la cual debe contener toda la informaci ́on sobre el grafo necesaria para correr su implementaci ́on. La
definici ́on interna de la esta estructura es a elecci ́on de ustedes y deber ́a soportar los m ́etodos que se describir ́an m ́as adelante,
m ́as los m ́etodos que ustedes consideren necesarios para implementar los algoritmos que esten implementando.
Entre los parametros debe haber como m ́ınimo los necesarios para guardar los datos de un grafo (vertices y lados) pero
ademas los necesarios para guardar el coloreo que se tiene hasta ese momento en el grafo y cualquier informaci ́on requerida
en los algoritmos a implementar.
IMPORTANTE: C ́omo Greedy usa un orden de los vertices, en esta estructura tiene que estar guardado
alg ́un orden de los vertices, y c ́omo vamos a cambiar ese orden repetidamente, debe ser algo que pueda ser
cambiado.
El coloreo siempre debe cumplir que si es un coloreo conqcoloresentonces los colores son 0 , 1 , ..., q−1. (en a ̃nos
anteriores pediamos colores entre 1 y la cantidad de colores, pero dado que los arrays en C comienzan en 0, algunos alumnos
cometieron errores. Para minimizar la posibilidad de errores pedimos de esta forma este a ̃no).


### 3.3 Grafo

es un puntero a una estructura de datosGrafoSt.

## 4 Funciones De Construcci ́on/Destrucci ́on/Copia del grafo

### 4.1 ConstruccionDelGrafo()

Prototipo de funci ́on:
Grafo ConstruccionDelGrafo();
La funci ́on aloca memoria, inicializa lo que haya que inicializar de una estructura GrafoSt ,lee un grafodesde standard
inputen el formato indicado en la secci ́on 9, lo carga en la estructura, incluyendo alg ́un orden de los vertices, corre Greedy
con ese orden para dejar todos los vertices coloreados y devuelve un puntero a la estructura.
En caso de error, la funci ́on devolver ́a un puntero aNULL, liberando cualquier memoria que haya alocado. Errores posibles
pueden ser fallas en alocar memoria, pero tambi ́en que el formato de entrada no sea v ́alido. En la secci ́on 9 se especifica el
formato. Entre otras cosas la primera linea que no sea comentario empieza con “p edge”y luego tiene indicados el n ́umeron
de v ́ertices y el n ́umeromde lados. Si la primera linea que no sea comentario no es asi debe imprimirse:
error en primera linea sin comentario
y retornarNULL.
A continuaci ́on de esa linea debe habermlineas cada una de las cuales indica un lado. Si no hay AL MENOSmlineas
de lados luego de esa, o si alguna de esas lineas no tiene el formato indicado en 9 o hay alg ́un problema con la lectura de una
de esas lineas, se debe imprimir
error de lectura en lado L
donde L es el n ́umero de linea donde se encuentra el error (en particular, si s ́olo hay`lineas de lados, con` < m, ahi debe
imprimirse “error de lectura en lado`+ 1”(pues en realidad el lado`+ 1 no existe).
Luego de ese mensaje de error, retornarNULL.
Si el total de v ́ertices extraidos de esasmlineas no esn, debe imprimir
cantidad de vertices leidos no es la declarada
y luego devolverNULL.
Pueden imprimir otros mensajes de error para alg ́un caso no contemplado ac ́a, por ejemplo por la forma de su estructura
interna, perosi el formato es el correcto no deben imprimir nada.

### 4.2 DestruccionDelGrafo()

Prototipo de funci ́on:
void DestruccionDelGrafo(Grafo G);
Destruye G y libera la memoria alocada.

### 4.3 CopiarGrafo()

Prototipo de funci ́on:
Grafo CopiarGrafo(Grafo G);
La funci ́on aloca memoria suficiente para copiar todos los datos guardados enG, hace una copia deGen esa memoria y
devuelve un puntero a esa memoria.
En caso de no poder alocarse suficiente memoria, la funci ́on devolver ́a un puntero aNULL.
Esta funci ́on se puede usar (y la usaremos asi en un main) para realizar una o mas copias deG, intentar diversas estrategias
de coloreo por cada copia, y quedarse con la que de el mejor coloreo.

## 5 Funciones de coloreo

### 5.1 Greedy()

Prototipo de funci ́on:
u32 Greedy(Grafo G);
Corre greedy en G con el orden interno que debe estar guardado de alguna forma dentro de G. Devuelve el numero de
colores que se obtiene.


### 5.2 Bipartito()

Prototipo de funci ́on:
int Bipartito(Grafo G);
Devuelve 1 siGes bipartito, 0 si no.
Si es bipartito, deja aGcoloreado en forma propia con los colores 0 y 1.
Si devuelve 0, antes debe colorear aGcon Greedy, en alg ́un orden.

## 6 Funciones para extraer informaci ́on de datos del grafo

Las tres funciones detalladas en esta seccion deben serO(1).

### 6.1 NumeroDeVertices()

Prototipo de funci ́on:
u32 NumeroDeVertices(Grafo G);
Devuelve el n ́umero de v ́ertices de G.

### 6.2 NumeroDeLados()

Prototipo de funci ́on:
u32 NumeroDeLados(Grafo G);
Devuelve el n ́umero de lados de G.

### 6.3 NumeroDeColores()

Prototipo de funci ́on:
u32 NumeroDeColores(Grafo G);
Devuelve la cantidad de colores usados en el coloreo que tiene en ese momento G. (luego de ConstruccionDelGrafo, G
siempre debe tener alg ́un coloreo propio, salvo en el medio de la corrida de Bipartito o de Greedy)

## 7 Funciones de los vertices

Las cinco funciones detalladas en esta seccion deben serO(1).

### 7.1 NombreDelVertice()

Prototipo de Funci ́on:
u32 NombreDelVertice(Grafo G, u32 i);
Devuelve el nombre real del v ́ertice n ́umeroien el orden guardado en ese momento en G. (el ́ındice 0 indica el primer
v ́ertice, el ́ındice 1 el segundo, etc)
Esta funci ́on no tiene forma de reportar un error (que se producir ́ıa siies mayor o igual que el n ́umero de v ́ertices), asi
que debe ser usada con cuidado.

### 7.2 ColorDelVertice()

Prototipo de Funci ́on:
u32 ColorDelVertice(Grafo G, u32 i);
Devuelve el color con el que est ́a coloreado el v ́ertice n ́umeroien el orden guardado en ese momento en G. (el ́ındice 0
indica el primer v ́ertice, el ́ındice 1 el segundo, etc).
Siies mayor o igual que el n ́umero de v ́ertices, devuelve 2^32 −1. (esto nunca puede ser un color en los grafos que testeeemos,
pues para que eso fuese un color, el grafo deberia tener al menos esa cantidad de vertices, lo cual lo hace inmanejable).


### 7.3 GradoDelVertice()

Prototipo de Funci ́on:
u32 GradoDelVertice(Grafo G, u32 i);
Devuelve el grado del v ́ertice n ́umeroien el orden guardado en ese momento en G. (el ́ındice 0 indica el primer v ́ertice,
el ́ındice 1 el segundo, etc).
Siies mayor o igual que el n ́umero de v ́ertices, devuelve 2^32 −1. (esto nunca puede ser un grado en los grafos que
testeeemos, pues para que eso fuese un grado de alg ́un v ́ertice, el grafo deberia tener al menos 2^32 vertices, lo cual lo hace
inmanejable).

### 7.4 ColorJotaesimoVecino()

Prototipo de funci ́on:
u32 ColorJotaesimoVecino(Grafo G, u32 i,u32 j);
Devuelve el color del v ́ecino numeroj(en el orden en que lo tengan guardado uds. en G, con el ́ındice 0 indicando el
primer v ́ecino, el ́ındice 1 el segundo, etc)) del v ́ertice n ́umeroien el orden guardado en ese momento en G. (el ́ındice 0 indica
el primer v ́ertice, el ́ındice 1 el segundo, etc)
Como en las otras funciones, el orden de LOS VERTICES guardado en G es el orden en que supuestamente vamos a correr
Greedy, pero el orden DE LOS VECINOS que usen ustedes es irrelevante, lo importante es que de esta forma podemos iterar
sobre los vecinos para realizar tests. (por ejemplo, para saber si el coloreo es o no propio).
Para que quede claro: si el orden en el que vamos a correr Greedy es 4,10,7,15,100 y los vecinos de 7 son 10,100,4,15 (en
ese orden) y los de 15 son 4,7,100 (en ese orden) y el color de 4 es 1, el de 7 es 2, el de 10 es 3, el de 15 es 4 y el de 100 es 0,
entonces:

- ColorJotaesimoVecino(G,2,0)=3 (pues 10 es el primer v ́ecino de 7)
- ColorJotaesimoVecino(G,2,2)=1 (pues 4 es el tercer v ́ecino de 7)
- ColorJotaesimoVecino(G,3,0)=1 (pues 4 es el primer v ́ecino de 15)
- ColorJotaesimoVecino(G,3,1)=2 (pues 7 es el segundo v ́ecino de 15)

Siies mayor o igual que el n ́umero de v ́ertices ojes mayor o igual que el n ́umero de vecinos del v ́erticei, devuelve 2^32 −1.
(esto nunca puede ser un color en los grafos que testeeemos, pues para que eso fuese un color, el grafo deberia tener al menos
esa cantidad de vertices, lo cual lo hace inmanejable).

### 7.5 NombreJotaesimoVecino()

Prototipo de funci ́on:
u32 NombreJotaesimoVecino(Grafo G, u32 i,u32 j);
Devuelve el nombre del v ́ecino numeroj(en el orden en que lo tengan guardado uds. en G, con el ́ındice 0 indicando el
primer v ́ecino, el ́ındice 1 el segundo, etc)) del v ́ertice n ́umeroien el orden guardado en ese momento en G. (el ́ındice 0 indica
el primer v ́ertice, el ́ındice 1 el segundo, etc)
Esta funci ́on no tiene forma de reportar un error (que se producir ́ıa siies mayor o igual que el n ́umero de v ́ertices ojes
mayor o igual que el n ́umero de vecinos dei), asi que debe ser usada con cuidado.

## 8 Funciones de ordenaci ́on

Estas funciones cambian el orden interno guardado en G que se usa para correr Greedy.

### 8.1 OrdenNatural()

Prototipo de funci ́on:
char OrdenNatural(Grafo G);
Ordena los vertices en orden creciente de sus “nombres” reales. (recordar que el nombre real de un v ́ertice es un u32)
retorna 0 si todo anduvo bien, 1 si hubo alg ́un problema.


### 8.2 OrdenWelshPowell()

Prototipo de funci ́on:
char OrdenWelshPowell(Grafo G);
Ordena los vertices de W de acuerdo con el orden Welsh-Powell, es decir, con los grados en ordenno creciente.
Por ejemplo si hacemos un for en i de GradoDelVertice(G,i) luego de haber corrido OrdenWelshPowell(G), deberiamos
obtener algo como 10,9,7,7,7,7,5,4,4.
El orden relativo entre vertices que tengan el mismo grado no se especifica, es a conveniencia de ustedes.
Retorna 0 si todo anduvo bien, 1 si hubo alg ́un problema.

### 8.3 SwitchVertices()

Prototipo de funci ́on:
char SwitchVertices(Grafo G,u32 i,u32 j);
Verifica quei, j <n ́umero de v ́ertices. Si no es cierto, retorna 1. Si ambos estan en el intervalo permitido, entonces
intercambia las posiciones de los vertices en los lugaresiyjdel orden interno deGqueGtenga en el momento que se llama
esta funci ́on. (numerados desde 0), y retorna 0.
Por ejemplo, si el orden interno en ese momento es 10,15,7,22,25,17,4 entonces SwitchVertices(G,4,2) deja el orden interno
10,15,25,22,7,17,4.

### 8.4 RMBCnormal()

(el nombre son las iniciales de ReordenManteniendoBloqueColores, normal) Prototipo de funci ́on:
char RMBCnormal(Grafo G);
Si G esta coloreado conrcolores yV C 1 son los vertices coloreados con 1,V C 2 los coloreados con 2, etc, entonces esta
funci ́on ordena los vertices poniendo primero los vertices deV C 1 , luego los deV C 2 , etc, hastaV Cr− 1. En otras palabras,
para todoi= 0, .., r−2 los vertices deV Ciestan todos antes de los vertices deV Ci+1.
Retorna 0 si todo anduvo bien, 1 si hubo alg ́un problema. (pej, si allocan memoria extra temporaria para realizar esta
funci ́on y el alloc falla, deben reportar 1).
A diferencia de las dos funciones que siguen, esta funci ́on por si sola es in ́util para bajar la cantidad de colores de algo
coloreado con Greedy, pero se usar ́a en conjunci ́on con SwitchColores (ver 8.7)

### 8.5 RMBCrevierte()

(el nombre son las iniciales de ReordenManteniendoBloqueColores, revierte) Prototipo de funci ́on:
char RMBCrevierte(Grafo G);
Si G esta coloreado conrcolores yV C 1 son los vertices coloreados con 1,V C 2 los coloreados con 2, etc, entonces esta
funci ́on ordena los vertices poniendo primero los vertices deV Cr− 1 , luego los deV Cr− 2 , luego los deV Cr− 3 , etc, hastaV C 1.
En otras palabras, para todoi= 0, .., r−2 los vertices deV Ciestan todos luego de los vertices deV Ci+1.
Retorna 0 si todo anduvo bien, 1 si hubo alg ́un problema. (pej, si allocan memoria extra temporaria para realizar esta
funci ́on y el alloc falla, deben reportar 1).

### 8.6 RMBCchicogrande()

(el nombre son las iniciales de ReordenManteniendoBloqueColores, chico-grande) Prototipo de funci ́on:
char RMBCchicogrande(Grafo G);
Si G esta coloreado conrcolores yV C 1 son los vertices coloreados con 1,V C 2 los coloreados con 2, etc, entonces esta
funci ́on ordena los vertices poniendo primero los vertices deV Cj 1 , luego los deV Cj 2 , etc, dondej 1 , j 2 , ..., jrson tales que
|V Cj 1 |≤|V Cj 2 |≤...≤|V Cjr|
Retorna 0 si todo anduvo bien, 1 si hubo alg ́un problema. (pej, si se alloca memoria extra temporaria para realizar esta
funci ́on y el alloc falla, deben reportar 1).

### 8.7 SwitchColores()

Esta funci ́on no reordena los vertices, pero permite obtener nuevos reordenamientos en combinaci ́on con las anteriores.
Prototipo de funci ́on:
char SwitchColores(Grafo G,u32 i,u32 j);
Verifica quei, j <n ́umero de colores que tieneGen ese momento. Si no es cierto, retorna 1. Si ambos estan en el intervalo
permitido, entonces intercambia los coloresi, j: todos los vertices que esten coloreados en el coloreo actual conipasan a tener


el colorjen el nuevo coloreo y los que estan coloreados conjen el coloreo actual pasan a tener el colorien el nuevo coloreo.
Los demas colores quedan como est ́an. Retorna 0 si todo se hizo bien.

## 9 Formato de Entrada

El formato de entrada ser ́a una variaci ́on de DIMACS, que es un formato estandard para representar grafos, con algunos
cambios.
La descripci ́onoficialde DIMACS es asi:

- Ninguna linea tiene mas de 80 caracteres. PERO hemos visto archivos DIMACS en la web que NO cumplen esta
    especificaci ́on, asi que NO la pediremos.
    Su c ́odigo debe poder procesar lineas con una cantidad arbitraria de caracteres.
- Al principio habr ́a cero o mas lineas que empiezan con c las cuales son lineas de comentario y deben ignorarse.
- Luego hay una linea de la forma:
    p edge n m
    donde n y m son dos enteros. Luego de m, y entre n y m, puede haber una cantidad arbitraria de espacios en blancos.
    El primer n ́umero (n) en teor ́ıa representa el n ́umero de v ́ertices y el segundo (m) el n ́umero de lados, pero hay ejemplos
    en la web en donde n es en realidad solo una COTA SUPERIOR del n ́umero de vertices. En nuestro caso, pediremos
    que n sea el n ́umero EXACTO de v ́ertices. Todos los grafos “v ́alidos”que nosotros usaremos para testear cumplir ́an
    que n ser ́a el n ́umero de vertices exacto, pero tambi ́en usaremos algunos grafos “no v ́alidos”para testear si su programa
    detecta que el formato no es correcto y devuelve un puntero aNULLen ese caso. (no cumplir con este requisito de
    detectar fallas no es motivo de desaprobaci ́on, ver 10.5).
- Luego siguen m lineas todas comenzando con e y dos enteros, representando un lado. Es decir, lineas de la forma:
    e v w
    (luego de “w” y entre “v” y “w” puede haber una cantidad arbitraria de espacios en blanco)
    Luego de esas m lineas debendetener la carga sin leer ninguna otra linea, a ́un si hay mas lineas. Estas lineas
    extras pueden tener una forma arbitraria, pueden o no ser comentarios, o extra lados, etc. y deben ser ignoradas.
    Pueden, por ejemplo, tener un SEGUNDO grafo, para que si esta funci ́on se llama dos veces por alg ́un programa, el
    programa cargue dos grafos.
    Por otro lado, el archivo puede efectivamente terminar en la ́ultima de esas lineas, y su c ́odigo debe poder procesar estos
    archivos tambi ́en. (en particular en otros a ̃nos hubo grupos que hicieron c ́odigo que no funcionaba si la ultima linea no
    era una linea en blanco. Esto es un error.)
    En un formato v ́alido de entrada habr ́a al menos m lineas comenzando con e, pero puede haber alg ́un archivo de testeo
    en el cual no haya al menos m lineas comenzando con e. En ese caso, como se especifica en 4.1, debe detenerse la carga
    y devolver un puntero a NULL.
    Ejemplo tomado de la web:
    c FILE: myciel3.col
    c SOURCE: Michael Trick (trick@cmu.edu)
    c DESCRIPTION: Graph based on Mycielski transformation.
    c Triangle free (clique number 2) but increasing
    c coloring number
    p edge 11 20
    e 1 2
    e 1 4
    e 1 7
    e 1 9
    e 2 3
    e 2 6
    e 2 8


```
e 3 5
e 3 7
e 3 10
e 4 5
e 4 6
e 4 10
e 5 8
e 5 9
e 6 11
e 7 11
e 8 11
e 9 11
e 10 11
```
- En algunos archivos que figuran en la web, en la lista pueden aparecer tanto un lado de la forma
    e 7 9
    como el
    e 9 7
    Los grafos que usaremos nosotrosno son asi.
    Es decir, ustedes pueden asumir en su programa que si aparece el lado e v w NO aparecer ́a el lado e w v. No es necesario
    que detecten si esto se cumple o no: no testearemos archivos que tengan lados e v w y e w v.
- Nunca fijaremosm= 0, es decir, siempre habr ́a al menos un lado. (y por lo tanto, al menos dos v ́ertices).
- En el formato DIMACS no parece estar especificado si hay algun limite para los enteros, pero en nuestro caso los
    limitaremos a enteros de 32 bits sin signo.
- Observar que en el ejemplo y en muchos otros casos en la web los vertices son 1,2,...,n, PERO ESO NO SIEMPRE
    SERA ASI. ́
    Que un grafo tenga el v ́erticevno implicar ́a que el grafo tenga el v ́erticev′conv′< v.
    Por ejemplo, los vertices pueden ser solo cinco, y ser 0, 1 , 10 , 15768 ,1000000. Ustedes deben pensar bien como van a
    resolver este problema.
- El orden de los lados no tiene porqu ́e ser en orden ascendente de los vertices.
    Ejemplo V ́alido:
    c vertices no consecutivos
    p edge 5 3
    e 1 10
    e 0 15768
    e 1000000 1

## 10 Final Warnings

### 10.1 Advertencias generales

- El uso de macros esta permitido pero como siempre, sean cuidadosos si los usan.
- Debe haber MUCHO COMENTARIO, las cosas deben entenderse.
    Si un proyecto se me hace dificil de entender, pasa al fondo de la pila de correcciones. Si se me hace muy d ́ıficil de
    entender, puede haber descuento de puntos. Si es muy inentendible, puede ser devuelto para que se corrija, o incluso
    ser desaprobado.
    Respecto de los nombres de las variables, s ́olo pido sentido com ́un. Por ejemplo, para denotar el n ́umero de v ́ertices
    pueden simplemente usar n, o bien una variable mas significativa como nvertices o si quieren usar un nombre como


```
“Superman”, no me opongo. Pero si al n ́umero de v ́ertices le llaman nlados, nl o ncolores entonces tendr ́an un gran
descuento de puntos. Idem con los nombres de las funciones. (pej, una funci ́on que se llame OrdenarVertices pero en
realidad ordene lados)
```
- No se puede incluir NINGUNA libreria externa que no sea una de las b ́asicas de C. (eg, stdlib.h, stdio.h, strings.h,
    stdbool.h, assert.h etc, si, pero otras no. Especificamente, glibc NO).
- Respecto de la RAM, no voy a poner este a ̃no un limite definitivo, pero si lo que hacen es una desmesura carente de
    sentido com ́un se les devolver ́a el proyecto para que lo corrijan.
- BE CAREFUL de no producir un stack overflow. En general los estudiantes provocan stack overflows haciendo una
    recursion demasiado profunda, o bien declarando un array demasiado grande. Si el tama ̃no del array depende de una
    variable que puede ser muy grande, usen el heap, no el stack. Por ejemplo, algo que dependa del n ́umero de vertices no
    debe ir al stack (ver el siguiente punto) mientras que algo que dependa del n ́umero de colores es posible que si puede ir
    al stack, pues no habr ́a grafos que usen mas de a lo sumo una decena de miles de colores. Sin embargo, testeen su uso,
    y recuerden que el tama ̃no del stack no es igual en Windows que en Linux y creo que en Mac tambien es distinto a los
    dos.
- El grafo de entrada puede tener miles, cientos de miles de vertices e incluso millones de vertices y lados. Testeen para
    casos grandes. En mi p ́agina hay algunos grafos de ejemplos y varios en otras paginas, y deber ́ıan hacer sus propios
    ejemplos.
- En general, usar linked lists es un error en este proyecto, pues suele producir demoras significativas para grafos grandes.
    Especialmente, grupos que usan una linked list para la lista de vecinos suelen encontrarse con problemas, dado que
    Greedy se usar ́a muchas veces y para cada v ́ertice debe recorrer al menos parcialmente la lista de vecinos. Si bien no
    esta prohibido usar linked lists, si las usan y ven que tienen problemas de tiempo, esto puede ser una causa.
- Recuerden KISS: Keep It Simple, Stupid. Intentar estructuras estravagantes lo mas probable es que les haga perder
    tiempo de programaci ́on sin beneficio claro. Por ejemplo, un par de grupos en a ̃nos anteriores intentaron usar bitmaps.
    Para qu ́e, no me qued ́o claro. Lo ́unico que lograron fue complicar el c ́odigo y aumentar la probabilidad de cometer
    errores, algo que efectivamente les pas ́o.
- Para hacer OrdenNatural, WelshPowell y los RMBC van a necesitar, obviamente, alguna funci ́on de ordenaci ́on. Tambi ́en
    pueden necesitar una funci ́on de ordenaci ́on en la construcci ́on del grafo. (esto no siempre es necesario, depende de
    c ́omo implementen la lectura de los datos).
    Recuerden quenpuede ser del orden de millones, asi que una funci ́onO(n^2 ) como bubble sort es un grave error.
    Al parecer no todos saben que existe una funci ́on en C llamada qsort (que NO ES quick sort y al menos en gcc es
    suficientemente r ́apida: en realidad el estandard de C no demanda nada sobre la complejidad de qsort, asi que alg ́un
    compilador podr ́ıa hacer que qsort fueseO(n^2 ), pero con gcc las cosas parecen andar bien). Quiz ́as les puede ser ́util
    en vez de programar ustedes su propia funci ́on de ordenaci ́on.
    WelshPowell,RMBCnormal y RMBCrevierte pueden ser implementados con un sortO(n). Si usan qsort es mas lento
    pero no mucho mas lento y es aceptable. (si lo programan bien, obvio)
    RMBCchicogrande tambi ́en puede ser implementado con un sortO(n) si se ordenan adecuadamente los colores primero,
    y esto ́ultimo puede ser hecho con qsort. Si se usa directamente qsort sobre los v ́ertices en vez de los colores, es mas
    lento pero no mucho mas lento y es aceptable.
    OrdenNatural no puede ser implementado con unsortO(n) pero puede ser implementado enO(n) si la estructura del
    grafo es construida adecuadamente y se hace un ordenamiento durante la construcci ́on del grafo. Pero si no se hace
    esto, usar qsort para OrdenNatural tambi ́en funciona adecuadamente.
- Testeen. Luego testeen un poco mas. Finalmente, testeen.
- Para el punto anterior deber ́an hacer por su cuenta uno o mas .c que incluyan un main que les ayude a testear sus
    funciones. (no deben entregar estos archivos)
- Deben testear la funcionalidad de cada una de las funciones que programan, con programas que testeen si las funciones
    efectivamente hacen lo que hacen o no.
    Programar sin errores es dif ́ıcil, y algunos errores se les pueden pasar a ́un siendo cuidadosos y haciendo tests, porque
    somos humanos. Pero hay errores que no deber ́ıan quedar en el c ́odigo que me entreguen, porque son errores que son
    f ́acilmente detectables con un m ́ınimo de sentido com ́un a la hora de testear.
- Tengan en cuenta que uds. deben programar esto de acuerdo a las especificaciones porque no saben qu ́e programa va
    a ser el que llame a sus funciones, ni c ́omo las va a usar. Asumir que ser ́an usadas de una forma que no est ́e en las
    especificaciones es un error grave.


### 10.2 Cosas que causan desaprobaci ́on autom ́atica

Las siguientes cosas provocan desaprobaci ́on autom ́atica no s ́olo por los errores en si, sino porque son cosas f ́acilmente
chequeables, asi que no se justifica entregar un proyecto sin haberlas chequeado. Esto no quiere decir que si no ocurre
ninguna esten aprobados, pueden desaprobar por otras cosas, en particular todos los a ̃nos hay alg ́un grupo que encuentra una
nueva forma de meter la pata en forma espectacular de una forma que no se me habia ocurrido.

1. El programa no compila. En particular, presten atenci ́on a los nombres de las funciones. Usen copypaste desde este
    documento para asegurarse que esten bien. Even better, ustedes van a tener que hacer uno o varios archivos test.c (o
    alg ́un otro nombre) con un main para testear su c ́odigo, y otros grupos tambien lo har ́an. Intercambien sus archivos
    test.c con los de otros grupos y verifiquen que su proyecto con su Rii.h compila con el test.c (o como se llame) de otro
    grupo.
2. No es capaz de leer archivos en el formato establecido. En particular, debe poder leer si se carga un grafo a mano
    por stdin pero tambien usando el operador de redirecci ́on “<”. Por ejemplo, si al compilar el archivo ejecutable es
    “ejec”entonces algo como ./ejec<KC debe funcionar si KC es un archivo. Si ejec tiene opciones de entrada, como por
    ejemplo el n ́umero de veces que se realiza Greedy, o una semilla de aleatoriedad, tambien debe andar, pej, algo como
    ./ejec 1000 154367<KC debe funcionar.
3. Greedy da coloreos no propios. Por ejemplo, en a ̃nos anteriores hubo grupos que coloreabanKncon menos dencolores.
4. Greedy da colores propios, pero al testear si el coloreo es propio o no la respuesta es que no porque tienen mal
    implementadas las funciones ColorDelVertice y ColorJotaesimoVecino. Tengan en cuenta que mis archivos de testeo no
    pueden acceder a su estructura interna, asi que la ́unica forma que tengo para testear si un coloreo es propio o no es
    usar esas funciones. Si la funci ́on de testeo de si un coloreo es propio me dice que no es propio, no me voy a poner
    a ver si el problema est ́a en Greedy o en las funciones ColorDelVertice y ColorJotaesimoVecino. (en otros a ̃nos si lo
    hac ́ıa, diferenciaba entre los dos casos, y le avisaba al grupo, pero es su responsabilidad testear bien asi que este a ̃no no
    diferenciar ́e entre esos casos).
5. Bipartito da un coloreo no propio, diciendo que un grafo es bipartito cuando no lo es.
6. Greedy da siempre la misma cantidad de colores para un grafo dado, independientemente del orden de los vertices. (hay
    grafos para los cuales greedy siempre colorea con la misma cantidad de colores, por ejemplo, los completos. Pero otros
    no. En mi p ́agina hay varios ejemplos de grafos para los cuales Greedy debe dar distinto n ́umero de colores dependiendo
    del orden).
7. Durante cualquiera de los RMBCs o SwitchColores seguido de alg ́un RMBC, la cantidad de colores aumenta respecto
    de la cantidad de colores que tenia antes de ese reordenamiento. En clase demostramos que esto no puede pasar, y es
    algo f ́acilmente testeable.
8. No es capaz de leer un grafo y hacer 1000 reordenamientos y Greedys en un tiempo razonable (razonable es alrededor
    de 15 minutos en una m ́aquina como las de Famaf, mas o menos, para los grafos mas grandes de la p ́agina. (pueden
    hacerse 3000 ordenamientos y Greedys en 5 minutos, asi que pedir 15 minutos para 1000 me parece razonable). Un par
    de horas no es razonable y una semana, menos. En particular he tenido alumnos que incluso con grafos con s ́olo un par
    de miles de lados demoraban mas de una hora en hacer UN Greedy. En este caso estan sumamente desaprobados).
9. Bipartito dice que alguno de los grafos bipartitos de mi p ́agina no es bipartito. Idem si no funciona bien con un grafo
    que tenga 4 vertices y dos lados alimentado directamente por stdin.
10. Buffer overflows, comportamiento indefinido o variables shadows.
11. Un memory leak. Puede no provocar desaprobaci ́on autom ́atica si no es muy grave pero si es suficientemente grande
provocar ́a desaprobaci ́on autom ́atica. Por ejemplo, hubo un a ̃no que un grupo entreg ́o un proyecto que para grafos
grandes agregaba algo asi como 10MB de RAM por cada corrida de Greedy y no la liberaba nunca.
12. En mi p ́agina habr ́a varios grafos de ejemplos. Un subconjunto de ellos ser ́an declarados de testeo obligatorio, es decir,
su programa debe si o si leerlos y procesarlos bien, sin producir segmentation faults, stacks overflows o alguna otra cosa
rara. Si algo asi ocurre, quedan desaprobados.

### 10.3 Cosas que pueden o no causar desaprobaci ́on del proyecto

1. Hay alguna interacci ́on defectuosa entre las funciones. Por ejemplo, un grupo entreg ́o un Bipartito que daba resultados
    incorrectos si se lo llamaba despues de OrdenWelshPowell, pero daba resultados correctos si se lo llamaba antes. El
    problema era que ellos tenian una estructura interna compleja y poco transparente y el que program ́o WelshPowell hizo
    que tocara una parte de esa estructura que el que program ́o Bipartito creia que no se iba a tocar despues del inicio.
    Este tipo de errores ser ́a evaluado caso por caso.


### 10.4 Cosas que no causan desaprobaci ́on autom ́atica pero si devoluci ́on del proyecto

1. Bipartito funciona bien con los grafos de mi p ́agina pero tiene alg ́un error que hace que para algunos grafos bipartitos
    diga que no lo son.
2. Los RMBCs ordenan por bloque de colores, pero no en el orden pedido. Por ejemplo, RMBCchicogrande ordena de
    grande a chico en vez de chico a grande o RMBCrevierte hace lo que deber ́ıa hacer RMBCnormal y viceversa.
3. OrdenNatural no ordena correctamente.
4. OrdenWelshPowell no ordena correctamente.
5. No procesa bien algunos grafos debido a un error en el procesamiento de las lineas de comentarios del grafo, pero anda
    bien si se eliminan los comentarios.
6. El programa anda bien con grafos chicos y medianos, pero produce un stack overflow en algunos grafos grandes que no
    son los de testeo obligatorio, ver el ́ultimo item de la enumeraci ́on de cosas que desaprueban.
7. Bipartito dice bien si un grafo es o no bipartito, pero retorna mal el n ́umero de componentes conexas.

### 10.5 No causa desaprobaci ́on ni devoluci ́on del proyecto pero si alg ́un descuento en la nota

1. El programa anda bien con archivos que cumplan las especificaciones, pero falla en devolverNULLsi el formato de
    entrada no es el correcto. Especificamente testearemos con archivos que declaren unmy luego no tengan al menosm
    lineas, o declaren unny el n ́umero de vertices no sean. Tambien con algunos en donde la primera linea que no sea
    comentario no empieza con “p edge”y cuando alguna de las lineas de lados no empieza con “e”.


