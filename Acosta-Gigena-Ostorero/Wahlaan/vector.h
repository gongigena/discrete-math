#ifndef VECTOR_H__
#define VECTOR_H__
#include "Rii.h"

typedef struct vector_t* vector;

vector vector_init();
unsigned int vector_size(vector);
vector vector_add(vector, int);
void vector_set(vector, int, int);
int vector_get(vector, int);
void vector_swap(vector, int, int);
void vector_free(vector);
vector vector_copy(vector);
void vector_clear(vector);
void vector_plusplus(vector, int);
void vector_sort(vector);
int vector_back(vector);
void vector_pop(vector);

//Auxiliar functions
void
vector_dump(vector);
#endif